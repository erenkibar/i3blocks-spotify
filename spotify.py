import dbus
import sys

def getNowPlaying():
    try:
        #Spotify interface
        bus = dbus.SessionBus()
        proxy = bus.get_object("org.mpris.MediaPlayer2.spotify","/org/mpris/MediaPlayer2")
        comm = dbus.Interface(proxy, dbus_interface='org.freedesktop.DBus.Properties')
        props = comm.Get('org.mpris.MediaPlayer2.Player','Metadata')
        artist = props['xesam:artist'][0]
        song = props['xesam:title']
        title = artist + ' - ' + song
        print(title)
    except dbus.exceptions.DBusException:
        sys.exit()

getNowPlaying()